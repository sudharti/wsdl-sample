class SampleController < ApplicationController
	
	 soap_service

    class CountriesType < WashOut::Type
      type_name 'CountriesType'
      map "string" => :string
    end

    class Country < WashOut::Type
      type_name 'Country'
      map "descrption" => CountriesType
    end

    class CountriesArray < WashOut::Type
      type_name 'CountriesArray'
      map "country" => [Country]
    end

    class PartyType < WashOut::Type
      type_name 'PartyType'
      map "string" => :string
    end

    class DestinationType < WashOut::Type
      type_name 'DestinationType'
      map "string" => :string
    end

    class PartyMemberArray < WashOut::Type
      type_name 'PartyMemberArray'
      map "party_member" => { "dob" => :datetime, "name" => :string, "surname" => :string, "policyholder" => :boolean }
    end

    class AttributesArray < WashOut::Type
      type_name 'AttributesArray'
      map "attribute" => [ "name" => :string, "value" => :string ]
    end

    class CoverDetailsArray < WashOut::Type
      type_name 'CoverDetailsArray'
      map "cover_detail" => [ cover_section: :string, cover_section_cost: :decimal,
                              cover_included: :boolean, cover_excess: { cover_excess_details: { excess_amount: :decimal },
                              voluntary_excess_details: { excess_amount: :decimal } } ]
    end

	  soap_action "travel_quote",
              :args   => { "travel_detail" => { "policy_type" => :string, "destination" => DestinationType, "countries" => [CountriesArray],
                            "trip_start_date" => :datetime, "trip_end_date" => :datetime,
                            "winter_sports_cover" => :boolean, "baggage_cover" => :boolean,
                            "cancellation_cover" => :boolean, "party_type" => PartyType,
                            "existing_medical_condition" => :boolean, "party_members" => [PartyMemberArray]
                             }},

              :return => { "results" => { "result" => { quote_expiry: :datetime, status_code: :string,
                              broker: :string, insurer: :string, reference: :string, 
                              policy_wording: :string,
                              recall: { link: :string, attributes: AttributesArray
                              },
                              premium: { annual: :decimal, monthly: :decimal, first_installment: :decimal,
                              installment: :decimal, deposit: :decimal, number_of_payments: :integer },
                              cover_details:  CoverDetailsArray
                          }
                        }
                      },
              :response_tag => 'travel_results'
  	
    def travel_quote

      travel_detail = "travel_detail"
      policy_type = params[travel_detail]["policy_type"]
      destination = params[travel_detail]["destination"]
      countries = params[travel_detail]["countries"]
      trip_start_date = params[travel_detail]["trip_start_date"]
      trip_end_date = params[travel_detail]["trip_end_date"]
      winter_sports_cover = params[travel_detail]["winter_sports_cover"]
      baggage_cover = params[travel_detail]["baggage_cover"]
      cancellation_cover = params[travel_detail]["cancellation_cover"]
      party_type = params[travel_detail]["party_type"]
      existing_medical_condition = params[travel_detail]["existing_medical_condition"]
      party_members = params[travel_detail]["party_members"]

      #Change hard coded values here
      
      response = { "results" => { "result" =>  { quote_expiry: Time.now, status_code: "Accepted",
                            broker: "Gold", insurer: "insurer_code", reference: "Z971-DAD-B74",
                            policy_wording: "policy wording", 
                            #recall: { link: "link", attributes: { :attribute => [ { :name => "Password", :value => "xxxxxxxxxxxxxx" }, { :name => "Password2", :value => "xxxxxxxxxxxxxx" } ] }
                            recall: { link: "link", attributes: { :attribute => [ { :name => "Password", :value => "xxxxxxxxxxxxxx" }] }
                            },
                            premium: { annual: 900.34, monthly: 0.0, first_installment: 0.0,
                            installment: 85.86, deposit: 90.03, number_of_payments: 11 },
                            cover_details: {
                              cover_detail: [ { cover_section: "Medical Cover", cover_section_cost: 0.0,
                              cover_included: true, cover_excess: {  cover_excess_details: { excess_amount: 0.0 }, 
                              voluntary_excess_details: { excess_amount: 0.0 } }},

                              { cover_section: "Baggage Cover", cover_section_cost: 0.0,
                              cover_included: true, cover_excess: {  cover_excess_details: { excess_amount: 0.0 }, 
                              voluntary_excess_details: { excess_amount: 0.0 } } }]
                            }
                      }
                    }
                  }
      render :soap => response
  	end

end
