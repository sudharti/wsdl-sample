xml.instruct!
xml.tag! "s:Envelope", "xmlns:s" => 'http://schemas.xmlsoap.org/soap/envelope/' do
  xml.tag! "s:Body" do
#    key = "tns:#{@operation}#{'Response'} welkin"

    xml.tag! @action_spec[:response_tag],  "xmlns" => "http://compareyourtravelinsurance.com/services/msm" do
      wsdl_data xml, result
    end
  end
end
