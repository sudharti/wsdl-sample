module WashOutHelper

  def wsdl_data_options(param)
    if param.name == 'GetProductsResult'
      { "xmlns:i" => "http://www.w3.org/2001/XMLSchema-instance" }
    end
    
    # case WashOut::Engine.style
    # when 'rpc'
    #   { :"xsi:type" => param.namespaced_type }
    # when 'document'
    #   { }
    # end
  end

  def wsdl_data(xml, params)
    params.each do |param|
      # if param.name == "GetProductsResult"
      #   tag_name = param.name.to_s + ' xmlns:i="http://www.w3.org/2001/XMLSchema-instance"'
      # else
        tag_name = param.name
      # end
      param_options = wsdl_data_options(param)
      
     if param.name == "int"
        puts "INTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT"
        puts "Name " + param.name.to_s
        puts "Type " + param.type.to_s
        puts "Multi" + param.multiplied.to_s
        puts "Value" + param.value.to_s
        puts "SScla" + param.source_class.class.to_s
        puts "Map"   + param.map.class.to_s
        
        param.value.last.each do |v|
          xml.tag! tag_name, v, param_options
        end
        break
      end
      
      

      if !param.struct?
        if !param.multiplied
          if param.value.nil?
            param_options = { "i:nil" => true }
          end      
          xml.tag! tag_name, param.value, param_options
        else
          param.value = [] unless param.value.is_a?(Array)
          
          param.value.each do |v|
            xml.tag! tag_name, v, param_options
          end
        end
      else
        
        if !param.multiplied
          puts "Checkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk"
          puts "Name " + param.name.to_s
          puts "Type " + param.type.to_s
          puts "Multi" + param.multiplied.to_s
          puts "Value" + param.value.to_s
          puts "SScla" + param.source_class.class.to_s
          puts "Map"   + param.map.class.to_s
          
          if param.map.class.to_s =="Hash"
            param_options = { "i:nil" => true }
          end
          xml.tag! tag_name,  param_options do
            wsdl_data(xml, param.map)
          end
        else          
          param.map.each do |p|
            xml.tag! tag_name, param_options do
              wsdl_data(xml, p.map)
            end
          end
        end
      end
    end
  end

  def wsdl_type(xml, param, defined=[])
    more = []
    if param.struct?
      puts param
      puts param.basic_type
      puts !defined.include?(param.basic_type)
      xml.tag! "xsd:complexType", :name => param.basic_type do
        xml.tag! "xsd:sequence" do
          param.map.each do |value|
            more << value if value.struct?
            xml.tag! "xsd:element", wsdl_occurence(value, false, :name => value.name, :type => value.namespaced_type)
          end
        end
      end
      if !defined.include?(param.basic_type)
        defined << param.basic_type
#      elsif !param.classified?
#      else
#        raise RuntimeError, "Duplicate use of `#{param.basic_type}` type name. Consider using classified types."
      end
    end

    more.each do |p|
      wsdl_type xml, p, defined
    end
  end

  def wsdl_occurence(param, inject, extend_with = {})
    data = !param.multiplied ? {} : {
#      "#{'xsi:' if inject}minOccurs" => 0,
#      "#{'xsi:' if inject}maxOccurs" => 'unbounded'
    }

    extend_with.merge(data)
  end
end
